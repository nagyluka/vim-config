filetype off
call pathogen#incubate()
call pathogen#helptags()

" Set indentation
set softtabstop=4 shiftwidth=4 expandtab 

" Set up code folding
set foldmethod=indent
set foldnestmax=10
set nofoldenable
set foldlevel=1

"Movement in splitted windows
map <c-j> <c-w>j
map <c-k> <c-w>k
map <c-l> <c-w>l
map <c-h> <c-w>h

"Set leader key
let mapleader=","

"Close window
map <leader>c :close<CR>


"Set key for tasklist (TODO tags / FIXME tags)
map <leader>td <Plug>TaskList

"Gungo bind - history with diff and returning back
map <leader>g :GundoToggle<CR>

"Enable syntax highligh
syntax enable
filetype on
filetype plugin indent on 

"pyflakes - unused imports check
let g:pyflakes_use_quickfix=0

"Validation of code - PEP8 
let g:pep8_map='<leader>8'

"Tab completition - 
au FileType python set omnifunc=pythoncomplete#Complete
let g:SuperTabDefaultCompletionType = "context"
set completeopt=menuone,longest,preview

"Ropevim - jumping in code, code completition
map <leader>j :RopeGotoDefinition<CR>
map <leader>r :RopeRename<CR>
"Set color
set background=dark
colorscheme koehler
"Set number
set number
set backspace=indent,eol,start "Backspace maže odsazení, konce řádků,...
set linebreak "zlom jen ve slově 
set rtp+={repository_root}/powerline/bindings/vim
"Set auto-write on buffer switch
set autowrite
"Enable 80 chars per line ruler
set colorcolumn=80
set guifont=Monospace\ 12
set pastetoggle=<F2>
"Enable project specific settings
set exrc
set secure
" Set F4 as Make keybind
nnoremap <F4> :make!<cr>
"Set F10 for NERDTree
nnoremap <F10> :NERDTree<cr>
